package com.sudhir.test.test.di

import com.sudhir.test.test.WebTests
import com.sudhir.test.web.ForecastApi
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [TestAppModule::class])
interface TestAppComponent {

    fun forecastRestApi(): ForecastApi

    fun inject(webTest: WebTests)
}