package com.sudhir.test.test.di

class TestInjectorFactory {

    companion object {
        fun newAppInjector(): TestAppComponent {
            return DaggerTestAppComponent.builder()
                .testAppModule(TestAppModule())
                .build()
        }
    }
}