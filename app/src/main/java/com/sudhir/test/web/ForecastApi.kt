package com.sudhir.test.web

import com.sudhir.test.web.dto.ForecastResponse
import com.sudhir.test.web.dto.PlacesSearchResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface ForecastApi {

    @GET("data/2.5/find")
    fun findCity(@Query("q") searchKey: String): Observable<PlacesSearchResponse>

    @GET("data/2.5/forecast")
    fun getForecast(@Query("id") cityId: Long): Observable<ForecastResponse>

}