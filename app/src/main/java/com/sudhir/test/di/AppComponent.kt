package com.sudhir.test.di

import com.sudhir.test.data.Repository
import com.sudhir.test.ui.CitiesListActivity
import com.sudhir.test.ui.ForecastActivity
import com.sudhir.test.ui.SearchActivity
import com.sudhir.test.viewmodel.SearchViewModel
import com.sudhir.test.viewmodel.ViewModelFactory
import com.sudhir.test.web.ForecastApi
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {

    fun provideForecastRestApi(): ForecastApi

    fun provideRepository(): Repository

    fun provideSearchViewModelFactory(): ViewModelFactory

    fun inject(searchViewModel: SearchViewModel)

    fun inject(searchActivity: SearchActivity)

    fun inject(citiesListActivity: CitiesListActivity)

    fun inject(forecastActivity: ForecastActivity)

}