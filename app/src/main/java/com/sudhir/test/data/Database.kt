package com.sudhir.test.data

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.sudhir.test.model.City
import com.sudhir.test.model.Forecast

@Database(entities = [City::class, Forecast::class], version = 1, exportSchema = false)
abstract class Database : RoomDatabase() {

    abstract fun cityDao(): CityDao

    abstract fun forecastDao(): ForecastDao
}