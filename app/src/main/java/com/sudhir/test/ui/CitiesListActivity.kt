package com.sudhir.test.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.sudhir.test.R
import com.sudhir.test.di.InjectorFactory
import com.sudhir.test.model.City
import com.sudhir.test.ui.adapter.CityAdapter
import com.sudhir.test.viewmodel.CitiesListViewModel
import com.sudhir.test.viewmodel.ViewModelFactory
import kotlinx.android.synthetic.main.activity_search.*
import java.util.*
import javax.inject.Inject

class CitiesListActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private lateinit var viewModel: CitiesListViewModel
    private val adapter = CityAdapter(this::onCityClick)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        InjectorFactory.newAppInjector(application).inject(this)
        initViewModel()
        initUi()
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(CitiesListViewModel::class.java)
        viewModel.cities.observe(this, Observer {
            adapter.swapData(it ?: Arrays.asList())
        })
    }

    private fun initUi() {
        setContentView(R.layout.activity_cities_list)
        recyclerView.setup(adapter)
    }

    private fun onCityClick(city: City) {
        viewModel.onCitySelected(city)
        val intent = Intent(this, ForecastActivity::class.java)
            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)
    }
}
