package com.sudhir.test.androidtest.di

class TestInjectorFactory {

    companion object {
        fun newAppInjector(): TestAppComponent {
            return DaggerTestAppComponent.builder()
                .testAppModule(TestAppModule())
                .build()
        }
    }
}
