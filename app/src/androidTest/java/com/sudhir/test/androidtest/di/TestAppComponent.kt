package com.sudhir.test.androidtest.di

import com.sudhir.test.androidtest.DaoTests
import com.sudhir.test.androidtest.RepositoryTests
import com.sudhir.test.androidtest.SettingsTests
import com.sudhir.test.androidtest.viewmodel.CitiesListViewModelTest
import com.sudhir.test.androidtest.viewmodel.SearchViewModelTest
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [TestAppModule::class])
interface TestAppComponent {

    fun inject(daoTest: DaoTests)

    fun inject(repositoryTest: RepositoryTests)

    fun inject(settingsTests: SettingsTests)

    fun inject(searchViewModelTest: SearchViewModelTest)

    fun inject(citiesListViewModelTest: CitiesListViewModelTest)
}